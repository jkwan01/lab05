//Jackson Kwan 1938023
package movies.importer;
import java.util.*;

public class LowercaseProcessor extends Processor{
	
	public LowercaseProcessor(String source, String dest) {
		super(source, dest, true);
	}
	
	public ArrayList<String> process(ArrayList<String> arrList){
		ArrayList<String> asLower = new ArrayList<String>();
		for (int i=0; i<arrList.size(); i++) {
			asLower.add(arrList.get(i).toLowerCase());
		}
		return asLower;
	}
	
	
}
